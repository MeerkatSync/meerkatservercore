// swift-tools-version:5.1

import PackageDescription

let package = Package(
    name: "MeerkatServerCore",
    platforms: [
        .iOS("13.0"),
        .macOS("10.15")
    ],
    products: [
        .library(
            name: "MeerkatServerCore",
            targets: ["MeerkatServerCore"]),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/MeerkatSync/meerkatcore", from: "1.0.0"),   
        .package(url: "https://github.com/vapor/service.git", from: "1.0.0"),

    ],
    targets: [
        .target(
            name: "MeerkatServerCore",
            dependencies: ["Service", "MeerkatCore"]),
        .testTarget(
            name: "MeerkatServerCoreTests",
            dependencies: ["MeerkatServerCore"]),
    ]
)
