//
//  ServerSyncController.swift
//  MeerkatCore
//
//  Created by Filip Klembara on 04/02/2020.
//

public protocol ServerSyncController: Service {
    var notificators: Notificators { get }
    var database: DBWrapper { get }

    func getUser(by id: UserID) -> Future<User>
    func removeUser(by id: UserID) -> Future<Void>
    func createUser(with id: UserID) -> Future<User>
    func push(_ transaction: Transaction, into groups: [Group], by user: User) -> Future<Diff>
    func pull(from groups: [Group], by user: User) -> Future<Diff>
    func getInfo(for user: User) -> Future<Info>
    func createGroup(with id: GroupID, objects: [ObjectDescription], by user: User) -> Future<Group>
    func removeGroup(with id: GroupID, by user: User) -> Future<Void>
    func subscribe(_ newMember: UserID, into group: GroupID, by user: User, as role: Role) -> Future<Void>
    func unsubscribe(_ oldMember: UserID, from group: GroupID, by: User) -> Future<Void>
    func users(for group: GroupID, by user: User) -> Future<[UserRole]>
}

extension ServerSyncController {
    public func notify(groups: Future<[Group]>) -> Future<Void> {
        let getUsersByGroup = groups.innerFlatMapFlatten { self.database.users(for: $0) }
        let users = getUsersByGroup.map { $0.flatMap { $0 } }
        return notify(users: users.map { $0.map { $0.user } })
    }

    public func notify(users: Future<[User]>) -> Future<Void> {
        let usersDir = users.map { Dictionary($0.map { ($0.id, $0) }) { a, b in a } }
        let uniqUsers = usersDir.map { $0.map { $0.value } }
        let send = notificators.notify(users: uniqUsers)
        return send
    }
}
