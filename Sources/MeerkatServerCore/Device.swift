//
//  Device.swift
//  MeerkatServerCore
//
//  Created by Filip Klembara on 07/02/2020.
//

public protocol Device {
    var id: String { get }
    var description: String { get }
}

extension Device {
    public var description: String {
        return ""
    }
}
