//
//  UserRole.swift
//  
//
//  Created by Filip Klembara on 28/04/2020.
//

public struct UserRole {
    public let user: User
    public let role: Role

    public init(user: User, role: Role) {
        self.role = role
        self.user = user
    }
}
