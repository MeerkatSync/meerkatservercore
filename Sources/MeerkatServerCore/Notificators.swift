//
//  Notificators.swift
//  MeerkatServerCore
//
//  Created by Filip Klembara on 07/02/2020.
//

public protocol Notificators: Service {
    var notificators: [Notificator] { get }
    var notificatorDBWrapper: NotificatorDBWrapper { get }
    func notify(users: Future<[User]>) -> Future<Void>
    func notify(device: Device, on worker: Worker) -> Future<Void>
}


extension Notificators {
    public func notify(users: Future<[User]>) -> Future<Void> {
        let devices = users.innerFlatMapFlatten { self.notificatorDBWrapper.devices(for: $0) }.map { $0.flatMap { $0 } }

        let sends = devices.innerFlatMapFlatten { self.notify(device: $0, on: devices.eventLoop) }
        return sends.transform(to: ())
    }

    public func notify(device: Device, on worker: Worker) -> Future<Void> {
        let notifs = notificators.filter { $0.canNotify(device) }
        let go = notifs.map { $0.sendNotification(to: device, on: worker) }.flatten(on: worker)
        return go.transform(to: ())
    }
}
