//
//  Future+innerMap.swift
//  MeerkatServerCore
//
//  Created by Filip Klembara on 06/02/2020.
//

extension Future where T: Collection {
    public func innerMap<U>(closure: @escaping (T.Element) throws -> U) -> EventLoopFuture<[EventLoopFuture<U>]> {
        map { arr -> [EventLoopFuture<U>] in
            arr.map { elem -> EventLoopFuture<U> in self.eventLoop.future(elem).map { try closure($0) } }
        }
    }

    public func innerMapFlatten<U>(closure: @escaping (T.Element) throws -> U) -> EventLoopFuture<[U]> {
        innerMap(closure: closure).flatMap { $0.flatten(on: self.eventLoop) }
    }
    
    public func innerFlatMap<U>(closure: @escaping (T.Element) throws -> EventLoopFuture<U>) -> EventLoopFuture<[EventLoopFuture<U>]> {
        map { arr -> [EventLoopFuture<U>] in
            arr.map { elem -> EventLoopFuture<U> in self.eventLoop.future(elem).flatMap { try closure($0) } }
        }
    }

    public func innerFlatMapFlatten<U>(closure: @escaping (T.Element) throws -> EventLoopFuture<U>) -> EventLoopFuture<[U]> {
        return innerFlatMap(closure: closure).flatMap { $0.flatten(on: self.eventLoop) }
    }
}

