//
//  DBWrapperError.swift
//  Async
//
//  Created by Filip Klembara on 06/02/2020.
//

public enum DBWrapperError: Error {
    case unknownUser(id: UserID)
    case unknownGroup(id: GroupID)
    case wrongPermissions(for: User, is: Role, minimum: Role)
    case groupAlreadyExists(id: GroupID)
    case userNotInGroup(user: User, group: Group)
    case databaseError(Error)
    case userAlreadyExists(id: UserID)
    case unknownObject(id: ObjectID)
}
