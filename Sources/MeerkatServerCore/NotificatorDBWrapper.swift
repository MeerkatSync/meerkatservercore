//
//  NotificatorDBWrapper.swift
//  MeerkatCore
//
//  Created by Filip Klembara on 04/02/2020.
//

public protocol NotificatorDBWrapper: Service {
    func add(device: Device, for user: User) -> Future<Void>
    func remove(deviceId: String, for user: User) -> Future<Void>
    func devices(for: User) -> Future<[Device]>
}
