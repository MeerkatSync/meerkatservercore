//
//  Notificator.swift
//  MeerkatCore
//
//  Created by Filip Klembara on 04/02/2020.
//

public protocol Notificator {
    func canNotify(_ device: Device) -> Bool
    func sendNotification(to device: Device, on: Worker) -> Future<Void>
}
