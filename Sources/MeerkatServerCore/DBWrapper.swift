//
//  DBWrapper.swift
//  MeerkatServerCore
//
//  Created by Filip Klembara on 06/02/2020.
//

import Service

public protocol DBWrapper: Service {
    func update(groups: [Group], with: Transaction, by: User) -> Future<Diff>
    func user(by: UserID) -> Future<User>
    func addUser(withId: UserID) -> Future<User>
    func remove(user: User) -> Future<Void>
    func users(for: Group) -> Future<[UserRole]>
    func diffs(for: User, in: [Group]) -> Future<Diff>
    func groups(for: User) -> Future<[Group]>
    func group(by: GroupID) -> Future<Group>
    func subscribe(_: User, to: Group, by: User, as: Role) -> Future<Void>
    func unsubscribe(_: User, from: Group, by: User) -> Future<Void>
    func addGroup(withId: GroupID, owner: User, objects: [ObjectDescription]) -> Future<Group>
    func remove(group: Group, by: User) -> Future<Void>
    func role(of: User, in: Group) -> Future<Role>
}
