import XCTest

import MeerkatServerCoreTests

var tests = [XCTestCaseEntry]()
tests += MeerkatServerCoreTests.allTests()
XCTMain(tests)
